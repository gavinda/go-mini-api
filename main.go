package main

import (
	"go-mini-api/users"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "hello there",
		})
	})
	r.GET("/users", func(c *gin.Context) {
		listUsers := users.List()
		c.JSON(200, gin.H{
			"status":  200,
			"message": "success",
			"data":    listUsers,
		})
	})
	r.Run(":2323") // listen and serve on 0.0.0.0:2323
}
