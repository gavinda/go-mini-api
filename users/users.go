package users

import (
	"fmt"

	"github.com/bxcodec/faker"
)

// User is user data information generated with faker
type User struct {
	Name  string `faker:"name"`
	Email string `faker:"email"`
}

// List is for showing user list
func List() []User {
	var users []User
	for i := 0; i < 5; i++ {
		var user User
		err := faker.FakeData(&user)
		if err != nil {
			fmt.Println(err)
		}
		users = append(users, user)
	}
	return users
}
