module go-mini-api

go 1.12

require (
	github.com/bxcodec/faker v2.0.1+incompatible
	github.com/gin-gonic/gin v1.4.0
)
